package com.sample.rxjavasample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "RxJava";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] alphabet = new String[]{"A", "B", "C", "D", "E", "F"};

        //Creating new Observable from array
        Observable observable = Observable.fromArray(alphabet);

        //Creating new Observer
        Observer observer = new Observer() {
            @Override
            public void onSubscribe(Disposable d) {
                Log.d(TAG, "onSubscribe: ");
            }

            @Override
            public void onNext(Object value) {
                Log.d(TAG, "onNext: " + value);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: " + e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: ");
            }
        };

        //observer subscribing to observable
        observable.subscribe(observer);

        //Creating observable using create operator
        Observable observableCreate = Observable.create(new ObservableOnSubscribe() {
            @Override
            public void subscribe(ObservableEmitter emitter) {

                try {

                    emitter.onNext("Some Value");

                    emitter.onComplete();

                } catch (Exception e) {

                    emitter.onError(e);
                }
            }
        });

        //same observer is being used here for subscribing to different Observables
        observableCreate.subscribe(observer);

        //creating observable using Interval operator
        Observable observableInterval = Observable.interval(1, TimeUnit.SECONDS);

        //creating Observable using Just keyword
        Observable observableJust = Observable.just(new String[]{"A", "B", "C", "D", "E", "F"});

        //creating Observable using Range keyword
        Observable observableRange = Observable.range(2,5);

        observableRange.subscribe(observer);




    }
}
